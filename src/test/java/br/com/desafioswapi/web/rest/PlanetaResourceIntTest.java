package br.com.desafioswapi.web.rest;

import br.com.desafioswapi.DesafioswapibackendApp;

import br.com.desafioswapi.domain.Planeta;
import br.com.desafioswapi.repository.PlanetaRepository;
import br.com.desafioswapi.service.PlanetaService;
import br.com.desafioswapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

import java.util.List;


import static br.com.desafioswapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PlanetaResource REST controller.
 *
 * @see PlanetaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DesafioswapibackendApp.class)
public class PlanetaResourceIntTest {

    private static final String DEFAULT_NOME = "Tatooine";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_CLIMA = "AAAAAAAAAA";
    private static final String UPDATED_CLIMA = "BBBBBBBBBB";

    private static final String DEFAULT_TERRENO = "AAAAAAAAAA";
    private static final String UPDATED_TERRENO = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMERO_FILMES = 5;

    @Autowired
    private PlanetaService planetaService;
    
    @Autowired
    private PlanetaRepository planetaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restPlanetaMockMvc;

    private Planeta planeta;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PlanetaResource planetaResource = new PlanetaResource(planetaRepository, planetaService);
        this.restPlanetaMockMvc = MockMvcBuilders.standaloneSetup(planetaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Planeta createEntity() {
        Planeta planeta = new Planeta()
            .nome(DEFAULT_NOME)
            .clima(DEFAULT_CLIMA)
            .terreno(DEFAULT_TERRENO);
        return planeta;
    }

    @Before
    public void initTest() {
        planetaRepository.deleteAll();
        planeta = createEntity();
    }

    @Test
    public void createPlaneta() throws Exception {
        int databaseSizeBeforeCreate = planetaRepository.findAll().size();

        // Create the Planeta
        restPlanetaMockMvc.perform(post("/api/planetas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planeta)))
            .andExpect(status().isCreated());

        // Validate the Planeta in the database
        List<Planeta> planetaList = planetaRepository.findAll();
        assertThat(planetaList).hasSize(databaseSizeBeforeCreate + 1);
        Planeta testPlaneta = planetaList.get(planetaList.size() - 1);
        assertThat(testPlaneta.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testPlaneta.getClima()).isEqualTo(DEFAULT_CLIMA);
        assertThat(testPlaneta.getTerreno()).isEqualTo(DEFAULT_TERRENO);
    }

    @Test
    public void createPlanetaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = planetaRepository.findAll().size();

        // Create the Planeta with an existing ID
        planeta.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlanetaMockMvc.perform(post("/api/planetas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planeta)))
            .andExpect(status().isBadRequest());

        // Validate the Planeta in the database
        List<Planeta> planetaList = planetaRepository.findAll();
        assertThat(planetaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllPlanetas() throws Exception {
        // Initialize the database
    	planetaService.save(planeta);

        // Get all the planetaList
        restPlanetaMockMvc.perform(get("/api/planetas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(planeta.getId())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].clima").value(hasItem(DEFAULT_CLIMA.toString())))
            .andExpect(jsonPath("$.[*].terreno").value(hasItem(DEFAULT_TERRENO.toString())))
        	.andExpect(jsonPath("$.[*].numeroFilmes").value(DEFAULT_NUMERO_FILMES));
    }
    
    @Test
    public void getPlaneta() throws Exception {
        // Initialize the database
    	planetaService.save(planeta);

        // Get the planeta
        restPlanetaMockMvc.perform(get("/api/planetas/{id}", planeta.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(planeta.getId()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.clima").value(DEFAULT_CLIMA.toString()))
            .andExpect(jsonPath("$.terreno").value(DEFAULT_TERRENO.toString()))
        	.andExpect(jsonPath("$.numeroFilmes").value(DEFAULT_NUMERO_FILMES.toString()));
    }
    
    @Test
    public void getPlanetaPorNome() throws Exception {
        // Initialize the database
    	planetaService.save(planeta);

        // Get the planeta
        restPlanetaMockMvc.perform(get("/api/planetas/nome/{nome}", planeta.getNome()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$[0].nome").value(DEFAULT_NOME.toString()));
    }
    
    @Test
    public void getNonExistingPlaneta() throws Exception {
        // Get the planeta
        restPlanetaMockMvc.perform(get("/api/planetas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updatePlaneta() throws Exception {
        // Initialize the database
    	planetaService.save(planeta);

        int databaseSizeBeforeUpdate = planetaRepository.findAll().size();

        // Update the planeta
        Planeta updatedPlaneta = planetaRepository.findById(planeta.getId()).get();
        updatedPlaneta
            .nome(UPDATED_NOME)
            .clima(UPDATED_CLIMA)
            .terreno(UPDATED_TERRENO);

        restPlanetaMockMvc.perform(put("/api/planetas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPlaneta)))
            .andExpect(status().isOk());

        // Validate the Planeta in the database
        List<Planeta> planetaList = planetaRepository.findAll();
        assertThat(planetaList).hasSize(databaseSizeBeforeUpdate);
        Planeta testPlaneta = planetaList.get(planetaList.size() - 1);
        assertThat(testPlaneta.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testPlaneta.getClima()).isEqualTo(UPDATED_CLIMA);
        assertThat(testPlaneta.getTerreno()).isEqualTo(UPDATED_TERRENO);
        assertThat(testPlaneta.getNumeroFilmes()).isNotEqualTo(DEFAULT_NUMERO_FILMES);
    }

    @Test
    public void updateNonExistingPlaneta() throws Exception {
        int databaseSizeBeforeUpdate = planetaRepository.findAll().size();

        // Create the Planeta

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPlanetaMockMvc.perform(put("/api/planetas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planeta)))
            .andExpect(status().isBadRequest());

        // Validate the Planeta in the database
        List<Planeta> planetaList = planetaRepository.findAll();
        assertThat(planetaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deletePlaneta() throws Exception {
        // Initialize the database
    	planetaService.save(planeta);

        int databaseSizeBeforeDelete = planetaRepository.findAll().size();

        // Delete the planeta
        restPlanetaMockMvc.perform(delete("/api/planetas/{id}", planeta.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Planeta> planetaList = planetaRepository.findAll();
        assertThat(planetaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Planeta.class);
        Planeta planeta1 = new Planeta();
        planeta1.setId("id1");
        Planeta planeta2 = new Planeta();
        planeta2.setId(planeta1.getId());
        assertThat(planeta1).isEqualTo(planeta2);
        planeta2.setId("id2");
        assertThat(planeta1).isNotEqualTo(planeta2);
        planeta1.setId(null);
        assertThat(planeta1).isNotEqualTo(planeta2);
    }
}
