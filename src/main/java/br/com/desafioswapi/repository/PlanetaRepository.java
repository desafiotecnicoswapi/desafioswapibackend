package br.com.desafioswapi.repository;

import br.com.desafioswapi.domain.Planeta;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the Planeta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlanetaRepository extends MongoRepository<Planeta, String> {
	List<Planeta> findByNome(String nome);
}
