package br.com.desafioswapi.domain;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Planeta.
 */
@Document(collection = "planeta")
public class Planeta implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    private String id;

    @Field("nome")
    private String nome;

    @Field("clima")
    private String clima;

    @Field("terreno")
    private String terreno;

    @Field("numero_filmes")
    private Integer numeroFilmes;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Planeta nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getClima() {
        return clima;
    }

    public Planeta clima(String clima) {
        this.clima = clima;
        return this;
    }

    public void setClima(String clima) {
        this.clima = clima;
    }

    public String getTerreno() {
        return terreno;
    }

    public Planeta terreno(String terreno) {
        this.terreno = terreno;
        return this;
    }

    public void setTerreno(String terreno) {
        this.terreno = terreno;
    }

    public Integer getNumeroFilmes() {
        return numeroFilmes;
    }

    public Planeta numeroFilmes(Integer numeroFilmes) {
        this.numeroFilmes = numeroFilmes;
        return this;
    }

    public void setNumeroFilmes(Integer numeroFilmes) {
        this.numeroFilmes = numeroFilmes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Planeta planeta = (Planeta) o;
        if (planeta.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), planeta.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Planeta{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", clima='" + getClima() + "'" +
            ", terreno='" + getTerreno() + "'" +
            ", numeroFilmes=" + getNumeroFilmes() +
            "}";
    }
}
