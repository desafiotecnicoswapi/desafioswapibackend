package br.com.desafioswapi.domain.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanetasDto {

	public int count;
	
	public String next;
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public List<PlanetaDto> getResults() {
		return results;
	}

	public void setResults(List<PlanetaDto> results) {
		this.results = results;
	}

	public List<PlanetaDto> results;
	
}
