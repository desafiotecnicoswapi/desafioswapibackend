package br.com.desafioswapi.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import br.com.desafioswapi.domain.Planeta;
import br.com.desafioswapi.domain.dto.PlanetaDto;
import br.com.desafioswapi.domain.dto.PlanetasDto;
import br.com.desafioswapi.repository.PlanetaRepository;

@Service
public class PlanetaService {

    private final Logger log = LoggerFactory.getLogger(PlanetaService.class);

    @Autowired
    PlanetaRepository planetaRepository;
    
    public int getNumeroFilmes(Planeta planeta) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = "https://swapi.co/api/planets/";
        List<PlanetaDto> todosPlanetas = new ArrayList<PlanetaDto>();
        while(url != null) {
            try {
                ResponseEntity<PlanetasDto> planetasDto = restTemplate.exchange(url, HttpMethod.GET, entity, PlanetasDto.class);
                if(planetasDto.hasBody()) {
                	url = planetasDto.getBody().getNext();
                	todosPlanetas.addAll(planetasDto.getBody().getResults());
                }	
            }catch(HttpClientErrorException e) {
            	//log.debug(e.getStackTrace());
            }
        }
        Optional<PlanetaDto> planetaCorrespondente = todosPlanetas.stream().filter(p -> p.getName().equals(planeta.getNome())).findFirst();
        if(planetaCorrespondente.isPresent()){
            return planetaCorrespondente.get().getFilms().length;
        }
        return 0;
		
    }
    
	public <S extends Planeta> S save(S entity) {
		entity.setNumeroFilmes(this.getNumeroFilmes(entity));
		return planetaRepository.save(entity);
	}
    
}
