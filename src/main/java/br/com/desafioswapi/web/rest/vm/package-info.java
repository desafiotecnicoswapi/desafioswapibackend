/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.desafioswapi.web.rest.vm;
