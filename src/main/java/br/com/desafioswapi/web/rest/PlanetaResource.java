package br.com.desafioswapi.web.rest;
import br.com.desafioswapi.domain.Planeta;
import br.com.desafioswapi.repository.PlanetaRepository;
import br.com.desafioswapi.service.PlanetaService;
import br.com.desafioswapi.web.rest.errors.BadRequestAlertException;
import br.com.desafioswapi.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Planeta.
 */
@RestController
@RequestMapping("/api")
public class PlanetaResource {

    private final Logger log = LoggerFactory.getLogger(PlanetaResource.class);

    private static final String ENTITY_NAME = "planeta";

    private final PlanetaService planetaService;
    
    private final PlanetaRepository planetaRepository;

    public PlanetaResource(PlanetaRepository planetaRepository, PlanetaService planetaService) {
        this.planetaRepository = planetaRepository;
        this.planetaService = planetaService;
    }

    /**
     * POST  /planetas : Create a new planeta.
     *
     * @param planeta the planeta to create
     * @return the ResponseEntity with status 201 (Created) and with body the new planeta, or with status 400 (Bad Request) if the planeta has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/planetas")
    public ResponseEntity<Planeta> createPlaneta(@RequestBody Planeta planeta) throws URISyntaxException {
        log.debug("REST request to save Planeta : {}", planeta);
        if (planeta.getId() != null) {
            throw new BadRequestAlertException("A new planeta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Planeta result = planetaService.save(planeta);
        return ResponseEntity.created(new URI("/api/planetas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /planetas : Updates an existing planeta.
     *
     * @param planeta the planeta to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated planeta,
     * or with status 400 (Bad Request) if the planeta is not valid,
     * or with status 500 (Internal Server Error) if the planeta couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/planetas")
    public ResponseEntity<Planeta> updatePlaneta(@RequestBody Planeta planeta) throws URISyntaxException {
        log.debug("REST request to update Planeta : {}", planeta);
        if (planeta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Planeta result = planetaService.save(planeta);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, planeta.getId().toString()))
            .body(result);
    }

    /**
     * GET  /planetas : get all the planetas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of planetas in body
     */
    @GetMapping("/planetas")
    public List<Planeta> getAllPlanetas() {
        log.debug("REST request to get all Planetas");
        return planetaRepository.findAll();
    }

    /**
     * GET  /planetas/:id : get the "id" planeta.
     *
     * @param id the id of the planeta to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the planeta, or with status 404 (Not Found)
     */
    @GetMapping("/planetas/{id}")
    public ResponseEntity<Planeta> getPlaneta(@PathVariable String id) {
        log.debug("REST request to get Planeta : {}", id);
        Optional<Planeta> planeta = planetaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(planeta);
    }
    /**
     * GET  /planetas/nome/:nome : get all the planetas by name.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of planetas in body
     */
    @GetMapping("/planetas/nome/{nome}")
    public List<Planeta> getAllPlanetasByNome(@PathVariable String nome) {
        log.debug("REST request to get all Planetas by name");
        List<Planeta> planetas = planetaRepository.findByNome(nome);
        return planetas;
    }

    /**
     * DELETE  /planetas/:id : delete the "id" planeta.
     *
     * @param id the id of the planeta to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/planetas/{id}")
    public ResponseEntity<Void> deletePlaneta(@PathVariable String id) {
        log.debug("REST request to delete Planeta : {}", id);
        planetaRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
